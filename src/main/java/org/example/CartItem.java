package org.example;

public class CartItem {
    private final Item item;
    private double quantity;

    public CartItem(Item item, double quantity) {
        this.item = item;
        this.quantity = quantity;
    }

    public double getSubtotal() {
        return item.getUnitPrice() * quantity;
    }

    public double getDiscountedSubtotal() {
        return getSubtotal() - getSaved();
    }

    public double getSaved() {
        if (item.isPromotion() && quantity >= 3) {
            return item.getUnitPrice();
        }
        return 0.0;
    }

    public Item getItem() {
        return item;
    }

    public String getReceiptInfo() {
        // "Name：Coca-Cola，Quantity：5 bottles，Unit Price：3.00(CNY)，Subtotal：15.00(CNY)\n" +
        return String.format("Name：%s，Quantity：%s %s，Unit Price：%.2f(CNY)，Subtotal：%.2f(CNY)\n",
                item.getName(), getQuantityString(), item.getUnit(quantity), item.getUnitPrice(), getSubtotal());
    }

    private String getQuantityString() {
        return item.isWeighed() ? String.format("%.1f", quantity).replace(".0", "") : String.format("%.0f", quantity);
    }

    public String getDiscountedReceiptInfo() {
        return String.format("Name：%s，Quantity：%s %s，Value：%.2f(CNY)\n",
                item.getName(), 1, item.getUnit(1), getSaved());
    }

    public void mergeWith(CartItem newCartItem) {
        this.quantity += newCartItem.quantity;
    }
}

package org.example;


import java.util.Map;

public class Printer {
    public String print(String[] cart, String[] promotions) {
        // load all items
        Map<String, String> itemsMap = PosDataLoader.loadAllItems();
        Map<String, Item> items = Item.loadItemList(itemsMap, promotions);
        Cart cartObject = Cart.loadCart(items, cart);

        StringBuilder sb = new StringBuilder();
        sb.append("***<No Profit Store> Shopping List***\n");
        sb.append("----------------------\n");
        // cart items
        cartObject.cartItems().forEach(cartItem -> sb.append(cartItem.getReceiptInfo()));
        // discounted items
        sb.append("----------------------\n");
        sb.append("Buy two get one free items：\n");
        cartObject.cartItems().stream()
                .filter(cartItem -> cartItem.getSaved() > 0.0)
                .forEach(cartItem -> sb.append(cartItem.getDiscountedReceiptInfo()));
        sb.append("----------------------\n");
        // get total
        double total = cartObject.cartItems().stream()
                .mapToDouble(CartItem::getDiscountedSubtotal)
                .sum();
        sb.append(String.format("Total：%.2f(CNY)\n", total));
        // get saved
        double saved = cartObject.cartItems().stream()
                .mapToDouble(CartItem::getSaved)
                .sum();
        sb.append(String.format("Saved：%.2f(CNY)\n", saved));
        sb.append("**********************\n");

        return sb.toString();
    }
}


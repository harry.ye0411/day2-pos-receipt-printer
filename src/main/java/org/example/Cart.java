package org.example;

import java.util.*;

public record Cart(List<CartItem> cartItems) {

    // parse cart items from String[]
    public static Cart loadCart(Map<String, Item> allItems, String[] cart) {
        // the item in String[] may be the same. if so, then add quantity
        // if not, then add new item
        Map<String, CartItem> cartItemMap = new LinkedHashMap<>();
        for (String itemStr : cart) {
            String barcode;
            double quantity = 1.0;
            if (itemStr.contains("-")) {
                String[] parts = itemStr.split("-");
                barcode = parts[0];
                quantity = Double.parseDouble(parts[1]);
            } else {
                barcode = itemStr;
            }
            Item item = allItems.get(barcode);
            if (item != null) {
                CartItem newCartItem = new CartItem(item, quantity);
                if (cartItemMap.containsKey(barcode)) {
                    CartItem cartItem = cartItemMap.get(barcode);
                    cartItem.mergeWith(newCartItem);
                } else {
                    cartItemMap.put(barcode, newCartItem);
                }
            }
        }
        return new Cart(new ArrayList<>(cartItemMap.values()));
    }
}

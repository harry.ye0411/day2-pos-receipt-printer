package org.example;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Item {
    private final String barcode;
    private final String name;
    private final double unitPrice;
    private final String unit;

    private final boolean isPromotion;

    public Item(String barcode, String name, double unitPrice, String unit, boolean isPromotion) {
        this.barcode = barcode;
        this.name = name;
        this.unitPrice = unitPrice;
        this.unit = unit;
        this.isPromotion = isPromotion;
    }

    // parse item from key-value pair
    public static Item parse(String key, String value, boolean isPromotion) {
        String[] parts = value.split(",");
        return new Item(key, parts[0], Double.parseDouble(parts[1]), parts[2], isPromotion);
    }

    // get item list from map
    public static Map<String, Item> loadItemList(Map<String, String> map, String[] promotions) {
        List<String> promotionList = Arrays.asList(promotions);
         return map.entrySet().stream()
                 .map(entry -> parse(entry.getKey(), entry.getValue(), promotionList.contains(entry.getKey())))
                 .collect(Collectors.toMap(Item::getBarcode, item -> item));
    }

    public String getBarcode() {
        return barcode;
    }

    public String getName() {
        return name;
    }

    public double getUnitPrice() {
        return unitPrice;
    }

    public String getUnit(double quantity) {
        return quantity > 1 ? unit + "s" : unit;
    }

    // is weighed
    public boolean isWeighed() {
        return unit.equals("pound");
    }

    public boolean isPromotion() {
        return isPromotion;
    }
}
